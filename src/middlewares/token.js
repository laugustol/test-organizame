import jwt from 'jsonwebtoken'

export default {
	encode: async (user) => {
		const token = jwt.sign({id:user.id}, 'secreto')
		return token
	},
	decode: async (req,res,next) =>{
		if(!req.headers.authorization) return res.status(401).json({message:'not auth code: 001'});
		const token = req.headers.authorization
		try{
			const { id } = await jwt.verify(token,'secreto')
			if(id){
				req.id = id
				next()
			}else{
				res.status(401).json({message:'not auth code: 002'})
			}	
		}catch(e){
			res.status(401).json({message:'not auth code: 003',error:e})
		}
	}
}