import express from "express"
import cors from "cors"
import bodyParser from 'body-parser'
import morgan from 'morgan'
import "dotenv/config"
import httpLib from 'http'

import routes from './modules/routes'

const app = express();
const http = httpLib.createServer(app);
app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/api',routes)


http.listen(process.env.PORT || 4000, () => {
	console.log(`Server on port ${process.env.PORT || 4000}`)
})