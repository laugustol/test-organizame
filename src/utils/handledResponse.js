export default class handledResponse {
	constructor(){
		this.path = ''
	}

	error(e){
		console.log("handledResponse Error",e)
		return {success: false, path: this.path, message: e}
	}

	response(data, message = ''){
		return {success: true, path: this.path, data, message: message }
	}
}