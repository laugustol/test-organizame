import handledResponse from '../../utils/handledResponse'
import {arrCategories} from '../category/controller'
import {Parser,parse} from 'json2csv'
const handleResponse = new handledResponse()

const arrProducts = [
    {
        id:1,
        sku:'ADMIN1',
        nameProduct: 'Administrator1',
		descriptionProduct: 'lorem ipsum1',
        price: '99',
		idCategory: 1
    }
]

export const listProducts = async (req,res) => {
	handleResponse.path = 'listProducts'
	const products = arrProducts
	res.json(handleResponse.response(products))
}

export const createProduct = async (req,res) => {
	handleResponse.path = 'createProduct'
	try{
		arrProducts.push({sku:req.body.sku, nameProduct: req.body.nameProduct, description: req.body.description, price: req.body.price, idCategory: req.body.idCategory});
		res.json(handleResponse.response(true))
	}catch(e){
		res.status(404).json(handleResponse.error(e))
	}
}

export const updateProduct = async (req,res) => {
	handleResponse.path = 'updateProduct'
	try{
        arrProducts.map(e => {
            if(e.id === req.params.id){
              e = req.body
            }
            return e
        })
		res.json(handleResponse.response(true))
	}catch(e){
		console.log(e)
		res.status(404).json(handleResponse.error(e))
	}
}

export const removeProduct = async (req,res) => {
	handleResponse.path = 'removeProduct'
	arrProducts.filter(e => e.id !== req.params.id)
	res.json(handleResponse.response(true))
}

export const getCsv = async (req,res) => {
	const newArr = []
	for(let i = 0; i<arrProducts.length; i++){
		let obj = {}
		arrCategories.forEach(e => {
			
			if(e.id == arrProducts[i].idCategory){
				obj = e
			}
		})
		newArr.push({
			sku: arrProducts[i].sku,
			nameProduct: arrProducts[i].nameProduct,
			descriptionProduct: arrProducts[i].descriptionProduct,
			price: arrProducts[i].price,
			nameShortCategory: obj.nameShort,
			nameCategory: obj.nameCategory,
			descriptionCategory: obj.description,
		})
	}
	console.log(newArr)
	const fields = ['sku', 'nameProduct', 'descriptionProduct', 'price', 'nameShortCategory', 'nameCategory', 'descriptionCategory'];
	const data = await parse(newArr, {fields} );
	res.attachment('filename.csv');
	res.status(200).send(data);
}