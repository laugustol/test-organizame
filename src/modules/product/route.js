import express from "express"
import {body, validationResult} from 'express-validator'

import token from '../../middlewares/token'
import {listProducts, createProduct, updateProduct, removeProduct, getCsv} from './controller'

const router = express()

router.get('/',token.decode,listProducts);
router.post('/',token.decode,body('sku').isLength({max:5}),
(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
},createProduct);
router.put('/:id',token.decode,body('sku').isLength({max:5}),
(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
},updateProduct);
router.delete('/:id',token.decode,removeProduct);
router.get('/get-csv',getCsv);

export default router