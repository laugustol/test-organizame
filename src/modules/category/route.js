import express from "express"
import {body, validationResult} from 'express-validator';

import token from '../../middlewares/token'
import {listCategories, createCategory, updateCategory, removeCategory} from './controller'

const router = express()

router.get('/',token.decode,listCategories);
router.post('/',token.decode,body('nameShort').isLength({max:5}),
(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
},createCategory);
router.put('/:id',token.decode,body('nameShort').isLength({max:5}),
(req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    next();
},updateCategory);
router.delete('/:id',token.decode,removeCategory);

export default router