import handledResponse from '../../utils/handledResponse'
const handleResponse = new handledResponse()

export const arrCategories = [
    {
        id:1,
        nameShort:'ADMIN2',
        nameCategory: 'Administrator2',
        description: 'lorem ipsum2'
    }
]

export const listCategories = async (req,res) => {
	handleResponse.path = 'listCategories'
	const categories = arrCategories
	res.json(handleResponse.response(categories))
}

export const createCategory = async (req,res) => {
	handleResponse.path = 'createCategory'
	try{
		arrCategories.push({nameShort:req.body.nameShort, nameCategory: req.body.nameCategory, description: req.body.description});
		res.json(handleResponse.response(true))
	}catch(e){
		res.status(404).json(handleResponse.error(e))
	}
}

export const updateCategory = async (req,res) => {
	handleResponse.path = 'updateCategory'
	try{
        arrCategories.map(e => {
            if(e.id === req.params.id){
              e = req.body
              
            }
            return e
        })
		res.json(handleResponse.response(true))
	}catch(e){
		console.log(e)
		res.status(404).json(handleResponse.error(e))
	}
}

export const removeCategory = async (req,res) => {
	handleResponse.path = 'removeCategory'
	arrCategories.filter(e => e.id !== req.params.id)
	res.json(handleResponse.response(true))
}