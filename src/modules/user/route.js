import express from "express"

import token from '../../middlewares/token'
import {login} from './controller'

const router = express()

router.post('/login',login)

export default router