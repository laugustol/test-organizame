import handledResponse from '../../utils/handledResponse'
import token from '../../middlewares/token'
const handleResponse = new handledResponse()

const arrUser = [
    {
        id:1,
        user: 'admin',
        password: '4321'
    },
    {
        id:2,
        user: 'user',
        password: '1234'
    },
]

export const login = async (req,res) => {
	handleResponse.path = 'login'
	const _user = {}
	arrUser.map(e => {
		if(e.user == req.body.user){
			_user.id = e.id;
			_user.user = e.user;
			_user.password = e.password;
		}
	});
	console.log(_user)
	if(_user){
		if(_user.password == req.body.password){
			const tokenGenerated = await token.encode(_user)
			delete _user.password
			res.json(handleResponse.response({..._user,token: tokenGenerated}, 'loginSuccess'))
		}else{
			res.json(handleResponse.error('invalidPassword'))
		}
	}else{
		res.json(handleResponse.error('invalidPassword'))
	}
}