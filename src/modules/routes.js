import express from "express";
import userRouter from './user/route'
import categoryRouter from './category/route'
import productRouter from './product/route'

const router = express();

router.use('/user',userRouter);
router.use('/category',categoryRouter);
router.use('/product',productRouter);

export default router;